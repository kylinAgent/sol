﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Text;
using UnityEditor;

public class UGUISpriteLinkBuilder : EditorWindow
{


    [MenuItem("UGUI Ex/SpriteLink Json Generator")]
    public static void ShowWindow()
    {
        EditorWindow window = EditorWindow.GetWindow(typeof(UGUISpriteLinkBuilder));

        window.maxSize = new Vector2(1413f, 904f);
        window.minSize = window.maxSize;
    }
    private GUIStyle myStyle = new GUIStyle();

    public string currentJsonFilePath = "";

    public string selectedBundleName;
    public string oldSelectedBundleName;
    public string searchString = "";
    public string low_SearchString;
    public string low_TempString;
    public string old_bundleName = "";

    public Color linkBGColor = Color.gray;

    Dictionary<string, List<UguiSpriteLink>> spriteLinkListDic = new Dictionary<string, List<UguiSpriteLink>>();
    Dictionary<string, List<UguiSpriteLink>> originSpriteLinkDic = new Dictionary<string, List<UguiSpriteLink>>();

    public List<UguiSpriteLink> selectedU_List = new List<UguiSpriteLink>();
    public List<UguiSpriteLink> U_List = new List<UguiSpriteLink>();
    public List<UguiSpriteLink> errorCheckPathList = new List<UguiSpriteLink>();
    public List<UguiSpriteLink> pathList = new List<UguiSpriteLink>();
    public List<string> errorList = new List<string>();
    public List<string> directoryPathList = new List<string>();


    public bool dicSearchCheck;

    public int listShowLevel = 20;
    public int linkListCountCheck = 0;

    public Vector2 scrollPosition;
    public Vector2 secondscrollPosition;

    private Color color;
    //public string longString = "This is a long-ish string";

    GUILayoutOption[] firstbox_options = { GUILayout.Width(600.0f), GUILayout.Height(700.0f) };
    GUILayoutOption[] secondbox_options = { GUILayout.Width(800.0f), GUILayout.Height(700.0f) };
    GUILayoutOption[] first_namebox_options = { GUILayout.Width(602.0f), GUILayout.Height(22.0f) };
    GUILayoutOption[] second_namebox_options = { GUILayout.Width(800.0f), GUILayout.Height(22.0f) };
    GUILayoutOption[] scroll_box_options = { GUILayout.Width(596), GUILayout.Height(692) };
    GUILayoutOption[] second_scroll_box_options = { GUILayout.Width(780), GUILayout.Height(686) };
    public Rect bundleWindowRect = new Rect(100, 100, 200, 200);

    private void OnGUI()
    {
        myStyle.fontSize = 18;
        myStyle.normal.textColor = Color.white;
        myStyle.alignment = TextAnchor.MiddleCenter;
        myStyle.padding = new RectOffset(6, 6, 12, 4);
        GUILayout.Label("SpriteLink Json Generator ver 1.0", myStyle);


        GUIStyle helpboxStyle = new GUIStyle(EditorStyles.helpBox);

        helpboxStyle.fontSize = 11;
        helpboxStyle.normal.textColor = Color.white;
        helpboxStyle.alignment = TextAnchor.MiddleLeft;
        helpboxStyle.padding = new RectOffset(6, 6, 6, 6);


        GUILayout.BeginVertical(helpboxStyle);
        //검색창
        GUILayout.Label(" 검색 ");
        searchString = EditorGUILayout.TextField(searchString);
        GUILayout.EndVertical();
        GUILayout.BeginHorizontal();
        BackgroundColorScope(Color.red);
        GUILayout.Box("Asset Bundle List", first_namebox_options);
        CloseBGColorScope();
        BackgroundColorScope(Color.white);
        GUILayout.Box("Sprite Link List", second_namebox_options);
        CloseBGColorScope();
        GUILayout.EndHorizontal();

        // -------------------------에셋번들 관련 상자들 시작----------------------
        GUILayout.BeginHorizontal();


        //<<<<<<<<<<<<<<<<<<<<< 에셋 번들 경로 리스트 표기 상자>>>>>>>>>>>>>>>>>>>>
        GUILayout.BeginVertical("helpBox", firstbox_options);

        // 스크롤바 시작
        scrollPosition = GUILayout.BeginScrollView(scrollPosition, scroll_box_options);

        var bundle_paths = AssetDatabase.GetAllAssetBundleNames();
        foreach (string bundle_path in bundle_paths)
        {
            low_TempString = bundle_path.ToLower();
            low_SearchString = searchString.ToLower();
            if (low_TempString.Contains(low_SearchString) || searchString.Length == 0 || CheckSearch(bundle_path))
            {
                ShowBundleList(bundle_path);
            }
        }
        GUILayout.EndScrollView();
        //스크롤바 끝

        GUILayout.EndVertical();
        //<<<<<<<<<<<<<<<<<<<<< 에셋 번들 경로 리스트 표기 상자 끝>>>>>>>>>>>>>>>>>



        // ======선택된 번들에 포함된 에셋의 스프라이트 링크 리스트 표기 상자=======
        GUILayout.BeginVertical("helpBox", secondbox_options);

        // 스크롤바(2) 시작
        GUILayout.BeginVertical("helpBox");
        secondscrollPosition = GUILayout.BeginScrollView(secondscrollPosition, second_scroll_box_options);

        if (oldSelectedBundleName != selectedBundleName)
        {
            secondscrollPosition.y = 0;
            oldSelectedBundleName = selectedBundleName;
        }
        old_bundleName = "";
        linkBGColor = Color.white;
        if (listShowLevel < selectedU_List.Count)
        {
            linkListCountCheck = 0;
            for (int i = 0; linkListCountCheck < listShowLevel; ++i)
            {

                dicSearchCheck = false;
                if (i >= selectedU_List.Count) break;
                UguiSpriteLink ulink = selectedU_List[i];


                if (ulink != null && ulink.redirectionCode == "" && originSpriteLinkDic.ContainsKey(ulink.assetCode))
                {
                    foreach (UguiSpriteLink link in originSpriteLinkDic[ulink.assetCode])
                    {
                        if (link.assetCode.ToLower().Contains(searchString.ToLower()))
                        {
                            dicSearchCheck = true;
                        }
                    }

                    if ((ulink.bundlePath.ToLower().Contains(searchString.ToLower()) || dicSearchCheck || ulink.assetCode.ToLower().Contains(searchString.ToLower()) || searchString.Length == 0))
                    {
                        ++linkListCountCheck;
                        GUILayout.BeginVertical();
                        if (old_bundleName != ulink.bundlePath)
                        {
                            old_bundleName = ulink.bundlePath;
                            if (linkBGColor == Color.white)
                            {
                                linkBGColor = Color.gray;
                            }
                            else
                            {
                                linkBGColor = Color.white;
                            }
                        }



                        ShowSpriteLinkInfo(ulink);
                        //CloseBGColorScope();
                        GUILayout.EndVertical();
                    }
                    //++linkListCountCheck;
                    //GUILayout.BeginVertical();
                    //ShowSpriteLinkInfo(ulink);
                    //GUILayout.EndVertical();
                }
                //if (i >= selectedU_List.Count) break;
            }
        }
        else
        {
            linkListCountCheck = 0;
            for (int i = 0; linkListCountCheck < selectedU_List.Count; ++i)
            {
                dicSearchCheck = false;
                if (i >= selectedU_List.Count) break;
                UguiSpriteLink ulink = selectedU_List[i];

                if (ulink != null && ulink.redirectionCode == "")
                {
                    foreach (UguiSpriteLink link in originSpriteLinkDic[ulink.assetCode])
                    {
                        if (link.assetCode.ToLower().Contains(searchString.ToLower()))
                        {
                            dicSearchCheck = true;
                        }
                    }

                    if ((ulink.bundlePath.ToLower().Contains(searchString.ToLower()) || dicSearchCheck || ulink.assetCode.ToLower().Contains(searchString.ToLower()) || searchString.Length == 0))
                    {
                        ++linkListCountCheck;

                        GUILayout.BeginVertical();
                        if (old_bundleName != ulink.bundlePath)
                        {
                            old_bundleName = ulink.bundlePath;
                            if (linkBGColor == Color.white)
                            {
                                linkBGColor = Color.gray;
                            }
                            else
                            {
                                linkBGColor = Color.white;
                            }
                        }

                        //BackgroundColorScope(linkBGColor);
                        ShowSpriteLinkInfo(ulink);
                        //CloseBGColorScope();
                        GUILayout.EndVertical();
                    }
                    //++linkListCountCheck;
                    //GUILayout.BeginVertical();
                    //ShowSpriteLinkInfo(ulink);
                    //GUILayout.EndVertical();
                }

            }
        }


        if (listShowLevel < selectedU_List.Count && linkListCountCheck == listShowLevel)
        {
            if (GUILayout.Button("Show More . . .", GUILayout.Height(22f)))
            {
                listShowLevel += 20;
                Repaint();
            }
        }


        GUILayout.EndScrollView();
        // 스크롤바 끝

        GUILayout.EndVertical();
        //박스끝

        GUILayout.BeginHorizontal();
        //EditorGUI.BeginDisabledGroup(selectedBundleName == null);
        //if (GUILayout.Button("Add New Sprite Link", GUILayout.Height(26f)))
        //{
        //    AddNewLink(selectedBundleName);
        //}
        //EditorGUI.EndDisabledGroup();
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
        // =====선택된 번들에 포함된 에셋의 스프라이트 링크 리스트 표기 상자 끝=====



        GUILayout.EndHorizontal();
        // --------------------------에셋번들 표기 끝------------------------------

        GUILayout.BeginHorizontal();
        if (errorList.Count == 0 && U_List.Count > 0)
        {
            BackgroundColorScope(Color.green);
        }
        else
        {
            BackgroundColorScope(Color.red);
        }
        EditorGUI.BeginDisabledGroup(errorList.Count > 0 || U_List.Count == 0);

        if (GUILayout.Button("Build to JSON", GUILayout.Height(28f)))
        {
            currentJsonFilePath = EditorUtility.OpenFolderPanel("Set JSON Build Folder"
                                                , currentJsonFilePath
                                                , null);
            if (currentJsonFilePath.Length != 0)
            {
                SaveSpriteLinkList(currentJsonFilePath);

            }

        }

        //if (GUILayout.Button("Build to JSON", GUILayout.Height(28f))) {
        //    SaveSpriteLinkList();
        //}
        EditorGUI.EndDisabledGroup();
        CloseBGColorScope();
        if (GUILayout.Button("Reload List by JSON", GUILayout.Height(28f)))
        {

            currentJsonFilePath = EditorUtility.OpenFilePanel("Reload List by JSON"
                                                , currentJsonFilePath
                                                , "json");
            if (currentJsonFilePath.Length != 0)
            {
                LoadSpriteLinkList(currentJsonFilePath);

            }
        }

        //if (GUILayout.Button("Reload List by JSON", GUILayout.Height(28f)))
        //{
        //    LoadSpriteLinkList();
        //}
        if (GUILayout.Button("Reset by AssetBundles", GUILayout.Height(28f)))
        {
            ResetSpriteLinkList();
        }


        GUILayout.EndHorizontal();


    }

    // 우측 스크롤바를 채우는 link 표현
    private void ShowSpriteLinkInfo(UguiSpriteLink link)
    {



        //GUILayout.BeginArea(new Rect(10, 10, 100, 100));
        Repaint();
        //GUILayout.BeginVertical("GroupBox");
        BackgroundColorScope(linkBGColor);
        GUILayout.BeginHorizontal("GroupBox");
        CloseBGColorScope();
        GUILayout.BeginVertical();
        //GUILayout.BeginHorizontal(GUILayout.Width(280f));
        //BackgroundColorScope(Color.cyan);
        //if (GUILayout.Button("Clone", GUILayout.Width(130f), GUILayout.Height(22f)))
        //{
        //    CloneLink(link);
        //}
        ////CloseBGColorScope();
        //EditorGUILayout.Space(4f); 
        //BackgroundColorScope(Color.magenta);
        //if (GUILayout.Button("Remove", GUILayout.Width(130f), GUILayout.Height(22f)))
        //{
        //    RemoveLinkList(link);
        //}
        //CloseBGColorScope();

        //GUILayout.EndHorizontal();
        //GUILayout.BeginHorizontal();





        if (GUILayout.Button("Bundle Path : " + link.bundlePath, "label", GUILayout.Height(20f), GUILayout.Width(580f)))
        {
            selectedBundleName = link.bundlePath;
            SetLinkList(link.bundlePath);

            GUI.FocusControl(null);
        }

        GUILayout.BeginHorizontal();
        GUILayout.Label("Code : " + link.assetCode, GUILayout.Width(540f));
        if (GUILayout.Button(" copy ", GUILayout.Width(38f), GUILayout.Height(18f)))
        {
            EditorGUIUtility.systemCopyBuffer = link.assetCode;
        }
        GUILayout.EndHorizontal();
        //link.assetCode = EditorGUILayout.TextField(link.assetCode, GUILayout.Width(243f));
        //GUILayout.EndHorizontal();
        GUILayout.Label("REdir :");

        //List<UguiSpriteLink> listLink = GetRedirList(link.assetCode);
        List<UguiSpriteLink> listLink = originSpriteLinkDic[link.assetCode];
        GUILayout.BeginVertical();
        for (int i = 1; i < listLink.Count; ++i)
        {
            GUILayout.BeginHorizontal();
            listLink[i].assetCode = EditorGUILayout.TextField(listLink[i].assetCode, GUILayout.Width(563f));
            if (GUILayout.Button(" - ", GUILayout.Width(18f), GUILayout.Height(18f)))
            {
                RemoveLinkList(listLink[i]);
            }
            GUILayout.EndHorizontal();
        }
        GUILayout.EndVertical();
        //link.redirectionCode = EditorGUILayout.TextField(link.redirectionCode, GUILayout.Width(243f));


        // 리다이렉션 주소 입력받는 UI 들어가야함

        // 방식 : 코드만 있고 리다이렉션 코드가 없다 : 원본취급. 이후 추가를 통해 만들어지는 요소들은??? 리다이렉션 코드만 있고 자기 코드가 없으 : 요걸 추가하게 됨. 여기서 추가된 애들이 몇개인지 리다이렉션 자동추가됨.


        //리다이렉션 주소 입력 UI끝


        //리다이렉션 주소 추가하는 버튼 들어가야함.
        if (GUILayout.Button("+ Add Redirection Code ", GUILayout.Height(20f)))
        {
            CloneLink(link);
        }

        GUILayout.EndVertical();

        var texturePath = AssetDatabase.GetAssetPathsFromAssetBundleAndAssetName(link.bundlePath, link.assetCode.Substring(link.assetCode.LastIndexOf("/") + 1));
        if (texturePath != null && texturePath.Length > 0)
        {
            DrawImageByPath(texturePath[0]);
        }
        else
        {

            BackgroundColorScope(Color.black);
            GUILayout.BeginVertical("Box");
            CloseBGColorScope();
            GUILayout.Label(" no image : \n   check the Real", GUILayout.Width(120f), GUILayout.Height(120f));
            GUILayout.EndVertical();
        }
        GUILayout.EndHorizontal();
        //GUILayout.EndVertical();
        //GUILayout.EndArea();
        Repaint();
    }



    private List<UguiSpriteLink> GetRedirList(string Code)
    {
        List<UguiSpriteLink> result = new List<UguiSpriteLink>();
        foreach (UguiSpriteLink link in U_List)
        {
            if (link.redirectionCode != "" && link.redirectionCode == Code)
            {
                result.Add(link);
            }
        }
        return result;
    }

    // 좌측 스크롤바를 채우는 list
    private void ShowBundleList(string path)
    {
        GUILayout.BeginHorizontal();
        if (path == selectedBundleName)
        {
            BackgroundColorScope(Color.blue);
        }
        else
        {
            BackgroundColorScope(Color.gray);
        }
        if (GUILayout.Button(path, GUILayout.Width(542f)))
        {
            if (selectedBundleName == path)
            {
                selectedBundleName = null;
                SetLinkList(null);
            }
            else
            {
                selectedBundleName = path;
                SetLinkList(path);
            }

            GUI.FocusControl(null);
        }
        CloseBGColorScope();
        if (U_List.Count == 0)
        {
            BackgroundColorScope(Color.gray);
            if (GUILayout.Button("none", GUILayout.Width(44f)))
            {

            }
            CloseBGColorScope();
        }
        else if (CheckError(path))
        {
            BackgroundColorScope(Color.red);
            if (GUILayout.Button("error", GUILayout.Width(44f)))
            {

            }
            CloseBGColorScope();
        }
        else
        {
            BackgroundColorScope(Color.green);
            if (GUILayout.Button("clear", GUILayout.Width(44f)))
            {

            }
            CloseBGColorScope();
        }

        GUILayout.EndHorizontal();
    }

    public bool CheckSearch(string path)
    {
        foreach (UguiSpriteLink check in U_List)
        {
            if (check.IsLinkInBundle(path) && ((check.redirectionCode != "" && check.redirectionCode.ToLower().Contains(searchString.ToLower())) || check.bundlePath.ToLower().Contains(searchString.ToLower()) || check.assetCode.ToLower().Contains(searchString.ToLower())))
            {
                return true;
            }
        }
        return false;
    }

    // 해당 path 에 없는 real경로가 있는지 확인(real경로는 같은 번들에 포함되어야 함)
    private bool CheckError(string path)
    {
        errorCheckPathList.Clear();
        pathList.Clear();
        foreach (UguiSpriteLink check in U_List)
        {
            pathList.Add(check);
            if (check.IsLinkInBundle(path) && check.redirectionCode != "")
            {
                errorCheckPathList.Add(check);
            }
        }
        foreach (UguiSpriteLink check in errorCheckPathList)
        {
            bool isthat = false;
            foreach (UguiSpriteLink check2 in pathList)
            {
                if (check.redirectionCode == check2.assetCode)
                {
                    isthat = true;
                    continue;
                }
            }
            if (!isthat)
            {
                if (!errorList.Contains(path))
                {
                    errorList.Add(path);
                }
                return true;
            }
        }
        if (errorList.Contains(path))
        {
            errorList.Remove(path);
        }
        return false;
    }
    private List<UguiSpriteLink> UpdateUList()
    {
        U_List.Clear();
        foreach (List<UguiSpriteLink> linkList in spriteLinkListDic.Values)
        {
            U_List.AddRange(linkList);
        }

        return U_List;
    }

    //dic 완료 1.0
    private void SetLinkList(string bundlepath = null)
    {
        selectedU_List.Clear();
        listShowLevel = 20;
        if (selectedBundleName != bundlepath) { listShowLevel = 20; }

        if (bundlepath == null)
        {
            UpdateUList();
            selectedU_List.AddRange(U_List);
        }
        else if (bundlepath != null && spriteLinkListDic.ContainsKey(bundlepath))
        {
            selectedU_List.AddRange(spriteLinkListDic[bundlepath]);
        }


        //foreach (UguiSpriteLink link in U_List)
        //{
        //    if (bundlepath == null)
        //    {
        //        selectedU_List.Add(link);
        //    }
        //    //selectedU_List.Add(link);
        //    else if (bundlepath != null && link.IsLinkInBundle(bundlepath))
        //    {

        //        UguiSpriteLink outLink = link.FindByBundlePath(bundlepath);
        //        //link.FindByBundlePath(bundlename);
        //        selectedU_List.Add(outLink);
        //    }
        //}
    }
    private void RemoveLinkList(UguiSpriteLink link)
    {
        U_List.Remove(link);
        //selectedU_List.Remove(link);
        originSpriteLinkDic[link.redirectionCode].Remove(link);
        spriteLinkListDic[link.bundlePath].Remove(link);
        SetLinkList(selectedBundleName);


    }

    private void RenewalListByAssetBundles()
    {

    }

    private void SaveSpriteLinkList(string savePath = "")
    {
        directoryPathList.Clear();

        if (savePath == "")
        {
            savePath = Application.dataPath;
        }
        currentJsonFilePath = savePath;

        foreach (List<UguiSpriteLink> list in spriteLinkListDic.Values)
        {
            foreach (UguiSpriteLink link in list)
            {
                //if (!directoryPathList.Contains(link.bundlePath))
                //{
                //    Directory.CreateDirectory(savePath + "/" + link.bundlePath, null);
                //    directoryPathList.Add(link.bundlePath);
                //}




                //링크의 모든 패스 돌면서 디렉토리 생성
                string linkJsonData = JsonUtility.ToJson(link, true);
                File.WriteAllText(savePath + "/" + "Ugui."+ link.assetCode.Replace("/",".") + ".json", linkJsonData);
                //File.WriteAllText(savePath + "/" + link.bundlePath + link.assetCode.Substring(link.assetCode.LastIndexOf("/")) + ".json", linkJsonData);
            }
        }



        //if (savePath == "")
        //{
        //    savePath = Application.dataPath + "~";
        //}
        List<UguiSpriteLink> linkList = new List<UguiSpriteLink>();

        linkList.Clear();
        foreach (List<UguiSpriteLink> list in spriteLinkListDic.Values)
        {
            linkList.AddRange(list);
        }

        //string jsonData2 = JsonUtility.ToJson(linkList[1]);
        string jsonData = JsonUtility.ToJson(new Serialization<UguiSpriteLink>(linkList), true);
        //List<SpriteLinkInfo> infoDatas = new List<SpriteLinkInfo>();
        //for(int i = 0; i < 100; ++i)
        //{
        //    SpriteLinkInfo infoData = new SpriteLinkInfo();
        //    infoData.testint = i;
        //    infoData.testint2 = i + 10;
        //    infoData.testintlist = new List<int> { i, i + 1 };
        //    infoData.teststring = i.ToString();
        //    infoDatas.Add(infoData);
        //}

        //var bytes = Object2MessagePack<List<SpriteLinkInfo>>(infoDatas);

        File.WriteAllText(savePath + "/" + "READJSONDATA.json", jsonData);


    }
    private void LoadSpriteLinkList(string filePath = "")
    {
        //Init
        U_List.Clear();
        selectedBundleName = null;
        selectedU_List.Clear();
        originSpriteLinkDic.Clear();
        spriteLinkListDic.Clear();

        listShowLevel = 20;







        if (filePath == "")
        {

            filePath = Application.persistentDataPath + "/testjsondata.json";
        }

        currentJsonFilePath = filePath;
        FileInfo info = new FileInfo(filePath);
        if (info.Exists)
        {
            string jsonData = File.ReadAllText(filePath);


            Serialization<UguiSpriteLink> serialization = JsonUtility.FromJson<Serialization<UguiSpriteLink>>(jsonData);

            U_List = serialization.ToList();
            foreach (UguiSpriteLink link in U_List)
            {
                //List<UguiSpriteLink> linkList = new List<UguiSpriteLink>();
                //linkList.Clear();
                //linkList.Add(link);
                if (link.redirectionCode == "" && !originSpriteLinkDic.ContainsKey(link.assetCode))
                {
                    List<UguiSpriteLink> linkList = new List<UguiSpriteLink>();
                    linkList.Clear();
                    linkList.Add(link);
                    originSpriteLinkDic.Add(link.assetCode, linkList);
                }
                else if (link.redirectionCode != "" && originSpriteLinkDic.ContainsKey(link.redirectionCode))
                {
                    originSpriteLinkDic[link.redirectionCode].Add(link);
                }
                if (!spriteLinkListDic.ContainsKey(link.bundlePath))
                {
                    List<UguiSpriteLink> linkList = new List<UguiSpriteLink>();
                    linkList.Clear();
                    linkList.Add(link);
                    spriteLinkListDic.Add(link.bundlePath, linkList);
                }
                else
                {
                    spriteLinkListDic[link.bundlePath].Add(link);
                }

            }
            SetLinkList();
        }
        else
        {
            Debug.Log("no File Json Sprite Link Data");
        }
        //string jsonData = File.ReadAllText(Application.persistentDataPath + "/testjsondata.json");


        //Serialization<UguiSpriteLink> serialization = JsonUtility.FromJson<Serialization<UguiSpriteLink>>(jsonData);

        //U_List = serialization.ToList();

        //foreach(UguiSpriteLink link in U_List)
        //{
        //    //List<UguiSpriteLink> linkList = new List<UguiSpriteLink>();
        //    //linkList.Clear();
        //    //linkList.Add(link);
        //    if (link.redirectionCode == "" && !originSpriteLinkDic.ContainsKey(link.assetCode))
        //    {
        //        List<UguiSpriteLink> linkList = new List<UguiSpriteLink>();
        //        linkList.Clear();
        //        linkList.Add(link);
        //        originSpriteLinkDic.Add(link.assetCode, linkList);
        //    }else if(link.redirectionCode != "" && originSpriteLinkDic.ContainsKey(link.redirectionCode))
        //    {
        //        originSpriteLinkDic[link.redirectionCode].Add(link);
        //    }
        //    if (!spriteLinkListDic.ContainsKey(link.bundlePath))
        //    {
        //        List<UguiSpriteLink> linkList = new List<UguiSpriteLink>();
        //        linkList.Clear();
        //        linkList.Add(link);
        //        spriteLinkListDic.Add(link.bundlePath, linkList);
        //    }
        //    else
        //    {
        //        spriteLinkListDic[link.bundlePath].Add(link);
        //    }

        //}
        //var readData = MessagePack2Object<List<SpriteLinkInfo>>(testBytes);

        //U_List = readData.UList;


        //byte[] testBytes = File.ReadAllBytes(Application.persistentDataPath + "/msgdata.bin");

        //var readData = MessagePack2Object<List<UguiSpriteLink>>(testBytes);

        //U_List = readData;
        //SetLinkList();
    }

    //public static T MessagePack2Object<T>(byte[] bytes)
    //{
    //    var serialize = MessagePackSerializer.Get<T>();
    //    var ms = new MemoryStream(bytes);
    //    var ret = serialize.Unpack(ms);
    //    ms.Close();
    //    return ret;
    //}
    //public static byte[] Object2MessagePack<T>(object obj)
    //{
    //    var serializer = MessagePackSerializer.Get<T>();
    //    var ms = new MemoryStream();
    //    serializer.Pack(ms, (T)obj);
    //    var ret = ms.ToArray();
    //    ms.Close();
    //    return ret;
    //}



    private List<UguiSpriteLink> ResetSpriteLinkList()
    {
        U_List.Clear();
        originSpriteLinkDic.Clear();
        spriteLinkListDic.Clear();
        selectedBundleName = null;
        selectedU_List.Clear();
        listShowLevel = 20;
        string s_name = null;


        var bundle_paths = AssetDatabase.GetAllAssetBundleNames();
        foreach (string bundle_path in bundle_paths)
        {
            var assets = AssetDatabase.GetAssetPathsFromAssetBundle(bundle_path);
            List<UguiSpriteLink> uLinkList = new List<UguiSpriteLink>();
            foreach (string asset in assets)
            {
                Sprite sprite = AssetDatabase.LoadAssetAtPath<Sprite>(asset);
                if (sprite != null && sprite is Sprite)
                {
                    s_name = sprite.name;


                    // 리소스 까지의 경로 삭제한 bundlepath + assetcode
                    string key = bundle_path.Replace("resources/resourcedata/", "");
                    key = key.Substring(0, key.LastIndexOf("/") + 1);

                    UguiSpriteLink uLink = new UguiSpriteLink(key + s_name, bundle_path);
                    uLinkList.Add(uLink);


                    //old
                    U_List.Add(uLink);

                    //for name linkList
                    if (originSpriteLinkDic.ContainsKey(uLink.assetCode))
                    {
                        Debug.Log("[ ! ]  중복 확인  : " + originSpriteLinkDic[uLink.assetCode][0].bundlePath + " 의  " + uLink.assetCode + " 와  " + uLink.bundlePath);
                    }
                    else
                    {
                        List<UguiSpriteLink> links = new List<UguiSpriteLink>();
                        links.Add(uLink);
                        originSpriteLinkDic.Add(uLink.assetCode, links);
                    }


                }
            }
            spriteLinkListDic.Add(bundle_path, uLinkList);
        }
        //기존 화면에 표시된 리스트를 지우고, 에셋번들을 읽어서 리스트로 만들어서 출력
        SetLinkList();
        return null;
    }

    public void CloneLink(UguiSpriteLink ulink)
    {
        UguiSpriteLink newUlink = new UguiSpriteLink("", ulink.bundlePath, ulink.assetCode);
        U_List.Add(newUlink);
        selectedU_List.Add(newUlink);
        originSpriteLinkDic[ulink.assetCode].Add(newUlink);
        spriteLinkListDic[ulink.bundlePath].Add(newUlink);
        //SetLinkList(selectedBundleName);


        //GUI.FocusControl(null);

        //secondscrollPosition.y = 99999;
    }


    // Utility
    // 가로선 긋기(색, 굵기, 패딩)
    public static void DrawHorizonLine(Color color, int thickness = 2, int padding = 10)
    {
        Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
        r.height = thickness;
        r.y += padding / 2;
        r.x -= 2;
        r.width += 6;
        EditorGUI.DrawRect(r, color);
    }

    public void DrawImageByPath(string path)
    {

        if (path != null && path.Length > 0)
        {
            Texture2D prefab = (Texture2D)AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D));

            BackgroundColorScope(Color.black);
            GUILayout.BeginVertical("Box");
            CloseBGColorScope();
            //GUILayout.FlexibleSpace();
            GUILayout.Label(prefab, GUILayout.Width(120f), GUILayout.Height(120f));
            //GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
        }


    }


    public void BackgroundColorScope(Color _color)
    {
        color = GUI.backgroundColor;
        GUI.backgroundColor = _color;
    }



    protected void CloseBGColorScope()
    {
        GUI.backgroundColor = color;
    }

}
