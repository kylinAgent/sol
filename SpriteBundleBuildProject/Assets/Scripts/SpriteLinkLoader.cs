﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SpriteLinkLoader : MonoBehaviour
{
    List<UguiSpriteLink> LinkList = new List<UguiSpriteLink>();
    public void LoadSpriteLinkList()
    {

        LinkList.Clear();

        FileInfo info = new FileInfo(Application.persistentDataPath + "/testjsondata.json");
        if (info.Exists)
        {
            string jsonData = File.ReadAllText(Application.persistentDataPath + "/testjsondata.json");


            Serialization<UguiSpriteLink> serialization = JsonUtility.FromJson<Serialization<UguiSpriteLink>>(jsonData);

            LinkList = serialization.ToList();
        }
        //string jsonData = File.ReadAllText(Application.persistentDataPath + "/testjsondata.json");


        //Serialization<UguiSpriteLink> serialization = JsonUtility.FromJson<Serialization<UguiSpriteLink>>(jsonData);

        //LinkList = serialization.ToList();
    }
    public UguiSpriteLink SpriteLink(string assetCode)
    {
        foreach(UguiSpriteLink link in LinkList)
        {
            if(link.assetCode == assetCode)
            {
                return link;
            }
        }
        return null;
    }
}
