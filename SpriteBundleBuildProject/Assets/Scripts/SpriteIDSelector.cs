﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SpriteIDSelector : MonoBehaviour
{
    [HideInInspector, SerializeField]
    public string ID = "";

    [SerializeField]
    public Image image;

    [SerializeField]
    public SpriteLinkLoader loader;


    [SerializeField]
    public LoadAssetBundle bundleLoader;

    [SerializeField]
    Sprite sprite;


    public void LoadImageByID(string id)
    {
        UguiSpriteLink link = loader.SpriteLink(id);
        if (link != null)
        {
            //bundleLoader.LoadBundle("AssetBundles/StandaloneWindows/" + link.bundlePath);
            if (bundleLoader.pathBundleDic.ContainsKey("AssetBundles/StandaloneWindows/" + link.bundlePath))
            {
                if(link.redirectionCode == null || link.redirectionCode == "")
                {
                    sprite = bundleLoader.pathBundleDic["AssetBundles/StandaloneWindows/" + link.bundlePath].LoadAsset<Sprite>(link.assetCode);
                }
                else
                {
                    sprite = bundleLoader.pathBundleDic["AssetBundles/StandaloneWindows/" + link.bundlePath].LoadAsset<Sprite>(link.redirectionCode);
                }

                image.sprite = sprite;
            }
        }
    }


}
