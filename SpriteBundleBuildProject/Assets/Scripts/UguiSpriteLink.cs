﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UguiSpriteLink
{
    // 에셋 이름
    [SerializeField] public string assetCode;
    // 번들 경로
    [SerializeField] public string bundlePath;
    // 진짜 지칭하는 에셋
    [SerializeField] public string redirectionCode;

    public UguiSpriteLink(string code, string bundle, string real)
    {
        this.assetCode = code;
        this.bundlePath = bundle;
        this.redirectionCode = real;
    }
    public UguiSpriteLink(string code, string bundle)
    {
        this.assetCode = code;
        this.bundlePath = bundle;
        this.redirectionCode = "";
    }

    public UguiSpriteLink FindByBundlePath(string bundlepath)
    {
        if(this.bundlePath == bundlepath)
        {
            return this;
        }
        return null;
    }
    public bool IsLinkInBundle(string bundlepath)
    {
        if (this.bundlePath == bundlepath)
        {
            return true;
        }
        return false;
    }
}
