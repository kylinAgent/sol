﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof (SpriteLinkLoader))]
public class SpriteLinkLoaderInspector : Editor
{
    public SpriteLinkLoader _component;

    
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();

        if (_component == null)
        {
            _component = (SpriteLinkLoader)target;
        }

        if(GUILayout.Button("Load Sprite Link Json", GUILayout.Width(Screen.width - 100f)))
        {
            _component.LoadSpriteLinkList();
        }
    }

}
