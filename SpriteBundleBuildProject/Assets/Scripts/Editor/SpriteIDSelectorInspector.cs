﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SpriteIDSelector))]
public class SpriteIDSelectorInspector : Editor
{
    public SpriteIDSelector _component;

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();

        if (_component == null)
        {
            _component = (SpriteIDSelector)target;
        }

        base.OnInspectorGUI();

        _component.ID = EditorGUILayout.TextField("Image Asset Code : ", _component.ID);

        if (GUILayout.Button("Load Sprite by AssetCode", GUILayout.Width(Screen.width - 100f)))
        {


            _component.LoadImageByID(_component.ID);
        }
    }
}
