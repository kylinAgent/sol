﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LoadAssetBundle))]
public class LoadAssetBundleInspector : Editor
{
    public LoadAssetBundle _component;


    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();

        if (_component == null)
        {
            _component = (LoadAssetBundle)target;
        }

        _component.bundlepath = EditorGUILayout.TextField("Path : ", _component.bundlepath);

        if (GUILayout.Button("Load AssetBundle", GUILayout.Width(Screen.width - 100f)))
        {
            _component.LoadBundle(_component.bundlepath);
        }
    }

}
