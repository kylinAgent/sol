﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class LoadAssetBundle : MonoBehaviour
{
    [HideInInspector, SerializeField]
    public string bundlepath = "";
    [HideInInspector, SerializeField]
    public Dictionary<string, AssetBundle> pathBundleDic = new Dictionary<string, AssetBundle>();
    // 비동기 방식으로 로드
    public void LoadBundle(string path)
    {
        StartCoroutine(DoLoadBundle(path));
    }
    IEnumerator DoLoadBundle(string path)
    {



        AssetBundleCreateRequest request = AssetBundle.LoadFromMemoryAsync(File.ReadAllBytes(path));

        //yield return request;

        if(request == null)
        {
            Debug.Log("실패!"); 
            yield return null;
        }
        else if (!pathBundleDic.ContainsKey(path))
        {
            pathBundleDic.Add(path, request.assetBundle);
        }
    }
}
